package com.example.dailysavings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;

import com.example.dailysavings.model.Expense;

public class CustomInputDialog extends Dialog implements AdapterView.OnItemSelectedListener {

    private TextView title, message;
    private EditText amount;
    private Spinner spinner;
    private ArrayAdapter<CharSequence> adapter;
    private String titleText;
    private Button submit;
    private String option = "na";
    private double value = 0;
    private NavController navController;
    private Expense expense;

    public CustomInputDialog(@NonNull Context context, int themeResId, String titleText, NavController navController) {
        super(context, themeResId);
        adapter = ArrayAdapter.createFromResource(context, R.array.options_array, android.R.layout.simple_spinner_item);
        this.titleText = titleText;
        this.navController = navController;
        expense = Expense.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(true);
        Window window = getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.custom_input_dialog);
        title = findViewById(R.id.title);
        title.setText(titleText);
        message = findViewById(R.id.message);
        amount = findViewById(R.id.amount);
        amount.setText(getValueFromExpense(titleText)+"");
        spinner = findViewById(R.id.spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titleText.equals("Next")) {
                    //Navigate to next fragment.
                    navController.navigate(R.id.action_expenditureFragment_to_savingsFragment);
                } else {
                    if (option.equals("na")) {
                        amount.setError("daily/weekly/monthly option has not been selected");
                        return;
                    }
                    if (TextUtils.isEmpty(amount.getText())) {
                        amount.setError("amount has not been entered");
                        return;
                    }
                    if (option.equals("Daily")) {
                        value = Double.parseDouble(amount.getText().toString()) * 30;
                    } else if (option.equals("Weekly")) {
                        value = Double.parseDouble(amount.getText().toString()) * 4;
                    } else {
                        value = Double.parseDouble(amount.getText().toString());
                    }
                    setValueToExpense(titleText, value);
                }
                dismiss();
            }
        });
        if (titleText.equals("Next")) {
            title.setText("Proceed to calculate savings!");
            message.setText("Press confirm to proceed to next or press back button to go back.");
            spinner.setVisibility(View.GONE);
            submit.setText("Confirm");
            String text = "\u20B9" + calculateTotalExpenditure();
            amount.setText(text);
            amount.setEnabled(false);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        option = adapter.getItem(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void setValueToExpense(String type, double value) {
        switch (type) {
            case "Dairy (Milk, Butter, Curd, etc.)":
                expense.setDairy(value);
                break;
            case "Meat, Fish & Eggs":
                expense.setMeatFish(value);
                break;
            case "Fruit & Vegetables":
                expense.setFruitVeg(value);
                break;
            case "Street Food":
                expense.setStreetFood(value);
                break;
            case "Cafe":
                expense.setCafe(value);
                break;
            case "Pub":
                expense.setPub(value);
                break;
            case "Restaurant":
                expense.setRestaurant(value);
                break;
            case "Clothes":
                expense.setClothes(value);
                break;
            case "Footwear":
                expense.setFootwear(value);
                break;
            case "Dal, Masala, Oil":
                expense.setDalMasalaOil(value);
                break;
            case "Bakery":
                expense.setBakery(value);
                break;
            case "Beverages":
                expense.setBeverages(value);
                break;
            case "Snacks & Branded food":
                expense.setSnacksBrandedFood(value);
                break;
            case "Beauty & Hygiene":
                expense.setBeautyHygiene(value);
                break;
            case "Household Goods":
                expense.setHouseholdGoods(value);
                break;
            case "Kitchen":
                expense.setKitchen(value);
                break;
            case "Babycare":
                expense.setBabycare(value);
                break;
            case "Transport (Outside)":
                expense.setTransportOutside(value);
                break;
            case "Transport (Intercity)":
                expense.setTransportInside(value);
                break;
            case "Others":
                expense.setOthers(value);
        }
    }

    public double getValueFromExpense(String type) {
        switch (type) {
            case "Dairy (Milk, Butter, Curd, etc.)":
                return expense.getDairy();

            case "Meat, Fish & Eggs":
                return expense.getMeatFish();

            case "Fruit & Vegetables":
                return expense.getFruitVeg();

            case "Street Food":
                return expense.getStreetFood();

            case "Cafe":
                return expense.getCafe();

            case "Pub":
                return expense.getPub();

            case "Restaurant":
                return expense.getRestaurant();

            case "Clothes":
                return expense.getClothes();

            case "Footwear":
                return expense.getFootwear();

            case "Dal, Masala, Oil":
                return expense.getDalMasalaOil();

            case "Bakery":
                return expense.getBakery();

            case "Beverages":
                return expense.getBeverages();

            case "Snacks & Branded food":
                return expense.getSnacksBrandedFood();

            case "Beauty & Hygiene":
                return expense.getBeautyHygiene();

            case "Household Goods":
                return expense.getHouseholdGoods();

            case "Kitchen":
                return expense.getKitchen();

            case "Babycare":
                return expense.getBabycare();

            case "Transport (Outside)":
                return expense.getTransportOutside();

            case "Transport (Intercity)":
                return expense.getTransportInside();

            case "Others":
                return expense.getOthers();
        }
        return 0;
    }

    public double calculateTotalExpenditure(){
        return expense.getDairy() + expense.getMeatFish() + expense.getFruitVeg() + expense.getStreetFood()
            + expense.getCafe() + expense.getPub() + expense.getRestaurant() + expense.getClothes() + expense.getFootwear()
                + expense.getDalMasalaOil() + expense.getBakery() + expense.getBeverages() + expense.getSnacksBrandedFood()
                + expense.getBeautyHygiene() + expense.getBeautyHygiene() + expense.getHouseholdGoods()+ expense.getKitchen()
                + expense.getBabycare() + expense.getTransportOutside() + expense.getTransportInside() + expense.getOthers();
    }
}

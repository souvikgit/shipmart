package com.example.dailysavings;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.dailysavings.model.Coordinates;
import com.example.dailysavings.model.Expense;
import com.example.dailysavings.model.Income;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * A simple {@link Fragment} subclass.
 */
public class SavingFromIncomeGraph extends Fragment implements View.OnTouchListener {

    private Income income;
    private Expense expense;
    private SeekBar sb10,sb20,sb30,sb50,sb75,sb100;
    private TextView amt10,amt20,amt30,amt50,amt75,amt100;
    private GraphView graphView1,graphView2,graphView3;
    private ArrayList<Coordinates> list1,list2,list3;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        income = Income.getInstance();
        expense = Expense.getInstance();
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        list3 = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_saving_from_income_graph, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        graphView1 = view.findViewById(R.id.graph1);
        graphView2 = view.findViewById(R.id.graph2);
        graphView3 = view.findViewById(R.id.graph3);
        sb10 = view.findViewById(R.id.seek_10);
        sb10.setProgress(10);
        sb10.setOnTouchListener(this::onTouch);
        sb20 = view.findViewById(R.id.seek_20);
        sb20.setProgress(20);
        sb20.setOnTouchListener(this::onTouch);
        sb30 = view.findViewById(R.id.seek_30);
        sb30.setProgress(30);
        sb30.setOnTouchListener(this::onTouch);
        sb50 = view.findViewById(R.id.seek_50);
        sb50.setProgress(50);
        sb50.setOnTouchListener(this::onTouch);
        sb75 = view.findViewById(R.id.seek_75);
        sb75.setProgress(75);
        sb75.setOnTouchListener(this::onTouch);
        sb100 = view.findViewById(R.id.seek_100);
        sb100.setProgress(100);
        sb100.setOnTouchListener(this::onTouch);
        amt10 = view.findViewById(R.id.seek_10_amt);
        amt20 = view.findViewById(R.id.seek_20_amt);
        amt30 = view.findViewById(R.id.seek_30_amt);
        amt50 = view.findViewById(R.id.seek_50_amt);
        amt75 = view.findViewById(R.id.seek_75_amt);
        amt100 = view.findViewById(R.id.seek_100_amt);
        setIncomeSaving();
        setExpenditureSaving();
        generateComparisonGraph();
    }

    private void setIncomeSaving() {
        double amount50,amount75,amount100;
        amount50 = income.getSalary() * 0.50;
        list1.add(new Coordinates(50,amount50));
        amount75 = income.getSalary() * 0.75;
        list1.add(new Coordinates(75,amount75));
        amount100 = income.getSalary();
        list1.add(new Coordinates(100,amount100));
        amt50.setText("50%    \u20B9"+amount50);
        amt75.setText("75%    \u20B9"+amount75);
        amt100.setText("100%  \u20B9"+amount100);
        drawGraph(graphView1,list1);
    }

    private void setExpenditureSaving() {
        double amount10,amount20,amount30;
        amount10 = expense.calculateTotalExpenditure() * 0.10;
        list2.add(new Coordinates(10,amount10));
        amount20 = expense.calculateTotalExpenditure() * 0.20;
        list2.add(new Coordinates(20,amount20));
        amount30 = expense.calculateTotalExpenditure() * 0.30;
        list2.add(new Coordinates(30,amount30));
        amt10.setText("10%    \u20B9"+amount10);
        amt20.setText("20%    \u20B9"+amount20);
        amt30.setText("30%    \u20B9"+amount30);
        drawGraph(graphView2,list2);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return true;
    }
    private void drawGraph(GraphView graphView,ArrayList<Coordinates> list) {
        graphView.removeAllSeries();
        ArrayList<DataPoint> dlist = new ArrayList<>();
        for (Coordinates i : list) {
            dlist.add(new DataPoint(i.getX(), i.getY()));
        }
        DataPoint[] arr = new DataPoint[dlist.size()];
        for (int i = 0; i < dlist.size(); i++) {
            arr[i] = dlist.get(i);
        }
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(arr);
        graphView.getViewport().setScalable(true);
        graphView.getViewport().setScalableY(true);
        graphView.getViewport().setScalable(true);
        graphView.getViewport().setScrollableY(true);
        graphView.getGridLabelRenderer().setHorizontalAxisTitle("Rate%");
        graphView.getGridLabelRenderer().setVerticalAxisTitle("Amount");
        graphView.getGridLabelRenderer().setHighlightZeroLines(true);
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(arr[dlist.size()-1].getX());
        graphView.getViewport().setMinY(0);
        graphView.getViewport().setMaxY(arr[dlist.size()-1].getY());
        graphView.addSeries(series);
    }
    public void generateComparisonGraph(){
        double savings = income.getSalary() - expense.calculateTotalExpenditure();
        if(savings>0){
            double monthly = savings*(Math.pow((1+(0.06/12)),1)) - savings;
            list3.add(new Coordinates(1,monthly));
            double yearly = savings*(Math.pow((1+(0.07/12)),12)) - savings;
            list3.add(new Coordinates(12,yearly));
            double yearly5 = savings*(Math.pow((1+(0.08/12)),12*5)) - savings;
            list3.add(new Coordinates(60,yearly5));
            double yearly10 = savings*(Math.pow((1+(0.1/12)),12*10)) - savings;
            list3.add(new Coordinates(120,yearly10));
            double yearly20 = savings*(Math.pow((1+(0.12/12)),12*20)) - savings;
            list3.add(new Coordinates(240,yearly20));
            drawGraph_v1(graphView3,list3);
        }
    }
    private void drawGraph_v1(GraphView graphView,ArrayList<Coordinates> list) {
        graphView.removeAllSeries();
        ArrayList<DataPoint> dlist = new ArrayList<>();
        for (Coordinates i : list) {
            dlist.add(new DataPoint(i.getX(), i.getY()));
        }
        DataPoint[] arr = new DataPoint[dlist.size()];
        for (int i = 0; i < dlist.size(); i++) {
            arr[i] = dlist.get(i);
        }
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(arr);
        graphView.getViewport().setScalable(true);
        graphView.getGridLabelRenderer().setHorizontalAxisTitle("Time in months");
        graphView.getGridLabelRenderer().setVerticalAxisTitle("Compund Interest");
        graphView.getGridLabelRenderer().setHighlightZeroLines(true);
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(arr[dlist.size()-1].getX());
        graphView.getViewport().setMinY(0);
        graphView.getViewport().setMaxY(arr[dlist.size()-1].getY());
        graphView.addSeries(series);
    }
}


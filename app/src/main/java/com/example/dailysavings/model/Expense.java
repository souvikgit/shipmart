package com.example.dailysavings.model;

public class Expense {
    private static Expense instance;
    private double dairy,meatFish,fruitVeg,streetFood,cafe,pub,restaurant,clothes,footwear,
            dalMasalaOil,Bakery,Beverages,SnacksBrandedFood,beautyHygiene,householdGoods,kitchen,
            babycare,transportOutside,transportInside,Others;

    public static Expense getInstance() {
        if(instance == null){
            instance = new Expense();
        }
        return instance;
    }

    public double getDairy() {
        return dairy;
    }

    public void setDairy(double dairy) {
        this.dairy = dairy;
    }

    public double getMeatFish() {
        return meatFish;
    }

    public void setMeatFish(double meatFish) {
        this.meatFish = meatFish;
    }

    public double getFruitVeg() {
        return fruitVeg;
    }

    public void setFruitVeg(double fruitVeg) {
        this.fruitVeg = fruitVeg;
    }

    public double getStreetFood() {
        return streetFood;
    }

    public void setStreetFood(double streetFood) {
        this.streetFood = streetFood;
    }

    public double getCafe() {
        return cafe;
    }

    public void setCafe(double cafe) {
        this.cafe = cafe;
    }

    public double getPub() {
        return pub;
    }

    public void setPub(double pub) {
        this.pub = pub;
    }

    public double getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(double restaurant) {
        this.restaurant = restaurant;
    }

    public double getClothes() {
        return clothes;
    }

    public void setClothes(double clothes) {
        this.clothes = clothes;
    }

    public double getFootwear() {
        return footwear;
    }

    public void setFootwear(double footwear) {
        this.footwear = footwear;
    }

    public double getDalMasalaOil() {
        return dalMasalaOil;
    }

    public void setDalMasalaOil(double dalMasalaOil) {
        this.dalMasalaOil = dalMasalaOil;
    }

    public double getBakery() {
        return Bakery;
    }

    public void setBakery(double bakery) {
        Bakery = bakery;
    }

    public double getBeverages() {
        return Beverages;
    }

    public void setBeverages(double beverages) {
        Beverages = beverages;
    }

    public double getSnacksBrandedFood() {
        return SnacksBrandedFood;
    }

    public void setSnacksBrandedFood(double snacksBrandedFood) {
        SnacksBrandedFood = snacksBrandedFood;
    }

    public double getBeautyHygiene() {
        return beautyHygiene;
    }

    public void setBeautyHygiene(double beautyHygiene) {
        this.beautyHygiene = beautyHygiene;
    }

    public double getHouseholdGoods() {
        return householdGoods;
    }

    public void setHouseholdGoods(double householdGoods) {
        this.householdGoods = householdGoods;
    }

    public double getKitchen() {
        return kitchen;
    }

    public void setKitchen(double kitchen) {
        this.kitchen = kitchen;
    }

    public double getBabycare() {
        return babycare;
    }

    public void setBabycare(double babycare) {
        this.babycare = babycare;
    }

    public double getTransportOutside() {
        return transportOutside;
    }

    public void setTransportOutside(double transportOutside) {
        this.transportOutside = transportOutside;
    }

    public double getTransportInside() {
        return transportInside;
    }

    public void setTransportInside(double transportInside) {
        this.transportInside = transportInside;
    }

    public double getOthers() {
        return Others;
    }

    public void setOthers(double others) {
        Others = others;
    }

    public double calculateTotalExpenditure(){
        return getDairy() + getMeatFish() + getFruitVeg() + getStreetFood()
                + getCafe() + getPub() + getRestaurant() + getClothes() + getFootwear()
                + getDalMasalaOil() + getBakery() + getBeverages() + getSnacksBrandedFood()
                + getBeautyHygiene() + getBeautyHygiene() + getHouseholdGoods()+ getKitchen()
                + getBabycare() + getTransportOutside() + getTransportInside() + getOthers();
    }
}

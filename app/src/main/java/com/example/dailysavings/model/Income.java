package com.example.dailysavings.model;

public class Income {
    private static Income instance;
    private double salary;

    public static Income getInstance() {
        if(instance == null){
            instance = new Income();
        }
        return instance;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}

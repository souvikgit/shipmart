package com.example.dailysavings;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dailysavings.model.Income;

public class AboutAppFragment extends Fragment implements View.OnClickListener {
    private NavController navController;
    private EditText salary;
    private ImageView aboutUs, contactUs;
    private Button go;
    private Income income;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        income = Income.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about_app, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        aboutUs = view.findViewById(R.id.about_us);
        contactUs = view.findViewById(R.id.contact_us);
        salary = view.findViewById(R.id.salary); //This is the editText where user will enter the salary
        go = view.findViewById(R.id.go); //On go button click it will go to the next section to ask for expenses
        aboutUs.setOnClickListener(this);
        contactUs.setOnClickListener(this);
        go.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_us:
                navController.navigate(R.id.action_aboutAppFragment_to_aboutUsFragment);
                break;
            case R.id.contact_us:
                navController.navigate(R.id.action_aboutAppFragment_to_contactUsFragment);
                break;
            case R.id.go:
                if(TextUtils.isEmpty(salary.getText().toString())){
                    salary.setError("Field cannot be left blank");
                }else{
                    income.setSalary(Double.parseDouble(salary.getText().toString()));
                    hideKeyboardFrom(getActivity(),v);
                }
                break;
        }
    }
    public void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        navController.navigate(R.id.action_aboutAppFragment_to_expenditureFragment);
    }
}

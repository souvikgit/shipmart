package com.example.dailysavings;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dailysavings.model.Expense;
import com.example.dailysavings.model.Income;


/**
 * A simple {@link Fragment} subclass.
 */
public class SavingsFragment extends Fragment implements View.OnClickListener{

    private Income income;
    private Expense expense;
    private TextView salaryTv,savingsTv;
    private CardView savingsFromIncome,savingsFromExpense;
    private NavController navController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        income = Income.getInstance();
        expense = Expense.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_savings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        salaryTv = view.findViewById(R.id.salary_amt);
        String Salary = "\u20B9"+income.getSalary();
        salaryTv.setText(Salary);
        String Savings = "\u20B9"+(income.getSalary() - expense.calculateTotalExpenditure())+"\n" +"("+income.getSalary() +"-" + expense.calculateTotalExpenditure()+")";
        savingsTv = view.findViewById(R.id.savings_amt);
        savingsTv.setText(Savings);
        savingsFromIncome = view.findViewById(R.id.card_graph1);
        savingsFromIncome.setOnClickListener(this::onClick);
        savingsFromExpense = view.findViewById(R.id.card_graph2);
        savingsFromExpense.setOnClickListener(this::onClick);
        navController = Navigation.findNavController(view);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.card_graph1:
                navController.navigate(R.id.action_savingsFragment_to_savingFromIncomeGraph);
                break;
            case R.id.card_graph2:
                navController.navigate(R.id.action_savingsFragment_to_savingsFromExpenseGraph);
                break;
        }
    }
}

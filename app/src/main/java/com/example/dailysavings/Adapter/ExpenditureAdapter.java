package com.example.dailysavings.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailysavings.R;

import java.util.ArrayList;

public class ExpenditureAdapter extends RecyclerView.Adapter<ExpenditureAdapter.HolderClass> {
    private ArrayList<String> label;
    private ArrayList<Integer> resource;
    private onCardClicked cardClicked;
    private int position;

    public ExpenditureAdapter(ArrayList<String> label, ArrayList<Integer> resource) {
        this.label = label;
        this.resource = resource;
    }

    @NonNull
    @Override
    public HolderClass onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.common_layout, parent, false);
        return new ExpenditureAdapter.HolderClass(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderClass holder, final int position) {
        holder.imageView.setImageResource(resource.get(position));
        holder.text.setText(label.get(position));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPosition(position);
                cardClicked.onClick(v);
            }
        });
    }

    @Override
    public int getItemCount() {
        return label.size();
    }

    public class HolderClass extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView text;
        CardView cardView;

        public HolderClass(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            text = itemView.findViewById(R.id.label);
            cardView = itemView.findViewById(R.id.card);
        }
    }

    public interface onCardClicked extends View.OnClickListener {
        @Override
        void onClick(View v);
    }

    public void clickListenerAction(onCardClicked listener) {
        cardClicked = listener;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}

package com.example.dailysavings;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dailysavings.Adapter.ExpenditureAdapter;
import com.example.dailysavings.model.Expense;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExpenditureFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> label;
    private ArrayList<Integer> resource;
    private ExpenditureAdapter expenditureAdapter;
    private String TAG = ExpenditureFragment.class.getSimpleName();
    private CustomInputDialog customInputDialog;
    public static double sumExpense = 0;
    private NavController navController;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        label = new ArrayList();
        resource = new ArrayList<>();
        label.add("Dairy (Milk, Butter, Curd, etc.)");
        label.add("Meat, Fish & Eggs");
        label.add("Fruit & Vegetables");
        label.add("Street Food");
        label.add("Cafe");
        label.add("Pub");
        label.add("Restaurant");
        label.add("Clothes");
        label.add("Footwear");
        label.add("Dal, Masala, Oil");
        label.add("Bakery");
        label.add("Beverages");
        label.add("Snacks & Branded food");
        label.add("Beauty & Hygiene");
        label.add("Household Goods");
        label.add("Kitchen");
        label.add("Babycare");
        label.add("Transport (Outside)");
        label.add("Transport (Intercity)");
        label.add("Others");
        label.add("Next");
        resource.add(R.drawable.ic_milk);
        resource.add(R.drawable.ic_meatfish);
        resource.add(R.drawable.ic_fruits_veg);
        resource.add(R.drawable.ic_street_food);
        resource.add(R.drawable.ic_cafe);
        resource.add(R.drawable.ic_pubs);
        resource.add(R.drawable.ic_resturant);
        resource.add(R.drawable.ic_clothes);
        resource.add(R.drawable.ic_footwear);
        resource.add(R.drawable.ic_dal_masala_oil);
        resource.add(R.drawable.ic_bakery);
        resource.add(R.drawable.ic_bevarages);
        resource.add(R.drawable.ic_snacks_branded_food);
        resource.add(R.drawable.ic_beauty_hygene);
        resource.add(R.drawable.ic_cleaning_household);
        resource.add(R.drawable.ic_kitchen);
        resource.add(R.drawable.ic_baby_care);
        resource.add(R.drawable.ic_transport_outstation);
        resource.add(R.drawable.ic_transport_intercity);
        resource.add(R.drawable.ic_others);
        resource.add(R.drawable.ic_next);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_expenditure, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_view);
        layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(layoutManager);
        expenditureAdapter = new ExpenditureAdapter(label, resource);
        recyclerView.setAdapter(expenditureAdapter);
        expenditureAdapter.notifyDataSetChanged();
        navController = Navigation.findNavController(view);
        expenditureAdapter.clickListenerAction(new ExpenditureAdapter.onCardClicked() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Clicked on " + label.get(expenditureAdapter.getPosition()));
                customInputDialog = new CustomInputDialog(getActivity(), R.style.WideDialog, label.get(expenditureAdapter.getPosition()), navController);
                customInputDialog.show();
            }
        });
    }
}
